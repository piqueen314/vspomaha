$(document).ready();

//Art Attractions
var arts402 = new google.maps.LatLng(41.284714, -96.006136);
var anderson = new google.maps.LatLng(41.254725, -95.931128);
var artistCoop = new google.maps.LatLng(41.256136, -95.929850);
var bemis = new google.maps.LatLng(41.252634, -95.932362);
var bluePom = new google.maps.LatLng(41.26068, -96.184940);
var creightonLied = new google.maps.LatLng(41.265909, -95.949765);
var dundeeGal = new google.maps.LatLng(41.265297, -95.989398);
var gallery72 = new google.maps.LatLng(41.233609, -95.940074);
var hilmer = new google.maps.LatLng(41.27626, -96.023774);
var hotShops = new google.maps.LatLng(41.269857, -95.933264);
var joslynArt = new google.maps.LatLng(41.260393, -95.945925);
var modernArts = new google.maps.LatLng(41.25941, -95.967286);
var passageway = new google.maps.LatLng(41.255797, -95.930269);
var phillipSchrager = new google.maps.LatLng(41.215983, -96.061319);
var sweatshop = new google.maps.LatLng(41.284, -96.008345);
var unoArt = new google.maps.LatLng(41.257766, -96.005995);
var whiteCrane = new google.maps.LatLng(41.255471, -95.930233);

//Culture Attractions
var apollon = new google.maps.LatLng(41.233234, -95.939889);
var bobKerrey = new google.maps.LatLng(41.265454, -95.924142);
var boysTown = new google.maps.LatLng(41.262678, -96.126792);
var carverBank = new google.maps.LatLng(41.28171, -95.947844);
var centurylink = new google.maps.LatLng(41.26375, -95.928406);
var dchs = new google.maps.LatLng(41.310169, -95.960074);
var elmuseo = new google.maps.LatLng(41.211893, -95.94795);
var filmStreams = new google.maps.LatLng(41.265838, -95.933671);
var freedomPark = new google.maps.LatLng(41.278392, -95.901351);
var geneLeahyMall = new google.maps.LatLng(41.257598, -95.93324);
var geraldFord = new google.maps.LatLng(41.245217, -95.960371);
var gpbhm = new google.maps.LatLng(41.26091, -95.959066);
var heartlandPark = new google.maps.LatLng(41.259019, -95.92735);
var hdMuseum = new google.maps.LatLng(41.256831, -96.117056);
var zoo = new google.maps.LatLng(41.227753, -95.928614);
var historicFlorence = new google.maps.LatLng(41.337299, -95.960963);
var houseOfLoom = new google.maps.LatLng(41.249882, -95.929609);
var jewell = new google.maps.LatLng(41.279559, -95.946706);
var joslynCastle = new google.maps.LatLng(41.261723, -95.971698);
var kaneko = new google.maps.LatLng(41.253349, -95.930728);
var kenefickPark = new google.maps.LatLng(41.234421, -95.916636);
var lauritzenGardens = new google.maps.LatLng(41.234421, -95.916636);
var lewisAndClark = new google.maps.LatLng(41.268486, -95.925197);
var lovesJazz = new google.maps.LatLng(41.281826, -95.947275);
var malcolmX = new google.maps.LatLng(41.291093, -95.964417);
var memorialPark = new google.maps.LatLng(41.26502, -96.005333);
var mormonTrail = new google.maps.LatLng(41.335009, -95.96497);
var mtVernon = new google.maps.LatLng(41.197539, -95.933593);
var childrensMuseum = new google.maps.LatLng(41.255018, -95.943007);
var pioneerPark = new google.maps.LatLng(41.259182, -95.937831);
var ralstonArena = new google.maps.LatLng(41.207009, -96.026371);
var sokolCzechMuseum = new google.maps.LatLng(41.201834, -95.942401);
var durham = new google.maps.LatLng(41.251439, -95.928344);

//Literature Attractions
var avsorensenLib = new google.maps.LatLng(41.263594, -95.986254);
var bensonLib = new google.maps.LatLng(41.285645, -96.00525);
var bessJohnsonLib = new google.maps.LatLng(41.278352, -96.233075);
var charlesBLib = new google.maps.LatLng(41.29988, -95.954747);
var florenceLib = new google.maps.LatLng(41.340756, -95.96084);
var millardLib = new google.maps.LatLng(41.227973, -96.120549);
var miltonRLib = new google.maps.LatLng(41.227973, -96.120549);
var saddlebrookLib = new google.maps.LatLng(41.311693, -96.147039);
var soLib = new google.maps.LatLng(41.20594, -95.955625);
var wClarkeLib = new google.maps.LatLng(41.2629, -96.053779);
var wDaleLib = new google.maps.LatLng(41.258118, -95.935192);
var willaCatherLib = new google.maps.LatLng(41.241162, -95.977866);

//Music Attractions
var barleyStreet = new google.maps.LatLng(41.284298, -96.008005);
var maha = new google.maps.LatLng(41.238444, -96.013977);
var oleavers = new google.maps.LatLng(41.245877, -95.989583);
var reverb = new google.maps.LatLng(41.285633, -96.00853);
var slowdown = new google.maps.LatLng(41.266547, -95.934153);
var sokol = new google.maps.LatLng(41.23849, -95.933738);
var waitingRoom = new google.maps.LatLng(41.285218, -96.008642);

//Theatre Attractions
var blueBarn = new google.maps.LatLng(41.253776, -95.930947);
var circle = new google.maps.LatLng(41.240519, -96.004369);
var funnyBone = new google.maps.LatLng(41.26068, -96.18494);
var holland = new google.maps.LatLng(41.258652, -95.931836);
var omahaPlayhouse = new google.maps.LatLng(41.26237, -96.019819);
var orpheum = new google.maps.LatLng(41.256086, -95.936878);
var ralstonTheatre = new google.maps.LatLng(41.19975, -96.049943);
var shelterbelt = new google.maps.LatLng(41.19975, -96.049943);
var backline = new google.maps.LatLng(41.256564, -95.93788);
var theRose = new google.maps.LatLng(41.257316, -95.94277);

//Arrays
var artArray = [arts402,anderson,artistCoop,bemis,bluePom,creightonLied,dundeeGal,gallery72,hilmer,hotShops,joslynArt,modernArts,passageway,phillipSchrager,sweatshop,unoArt,whiteCrane];
var artNameArray = ['402 Arts Collective','Anderson OBrien Fine Art','Artist Cooperative Gallery','Bemis Center for Contemorary Arts','Blue Pomegranate','Creighton Lied Art Gallery','Dundee Gallery','Gallery 72','Hilmer Art Gallery','Hot Shops Art Center','Joslyn Art Museum','Modern Arts Midtown','Passageway Gallery','Phillip Schrager Collection of Contemorary Art','Sweatshop Gallery','UNO Art Gallery','White Crane Gallery'];

var cultureArray = [apollon,bobKerrey,boysTown,carverBank,centurylink,dchs,elmuseo,filmStreams,freedomPark,geneLeahyMall,geraldFord,gpbhm,heartlandPark,hdMuseum,zoo,historicFlorence,houseOfLoom,jewell,joslynCastle,kaneko,kenefickPark,lauritzenGardens,lewisAndClark,lovesJazz,malcolmX,memorialPark,mormonTrail,mtVernon,childrensMuseum,pioneerPark,ralstonArena,sokolCzechMuseum,durham];
var cultureNameArray = ['Apollon','Bob Kerrey Pedestrian Bridge','Boys Town Village','Carver Bank','CenturyLink Convention Center','Douglas County Historical Society','El Museo Latino','Film Streams','Freedom Park','Gene Leahy Mall','Gerald R. Ford Birthsite and Gardens','Great Plains Black History Museum','Heartland of America Park','Henry & Dorothy Riekes Museum','Henry Doorly Zoo','Historic Florence Bank','House of Loom','Jewell Building','Joslyn Castle','Kaneko','Kenefick Park','Lauritzen Gardens','Lewis & Clark Landing','Loves Jazz and Arts Center','Malcolm X Birthsite','Memorial Park','Mormon Trail Center at Historic Winter Gardens','Mt. Vernon Gardens','Omaha Childrens Museum','Pioneer Courage Park','Ralston Arena','Sokol South Omaha Czechoslovak Museum','The Durham Museum'];

var litArray = [avsorensenLib,bensonLib,bessJohnsonLib,charlesBLib,florenceLib,millardLib,miltonRLib,saddlebrookLib,soLib,wClarkeLib,wDaleLib,willaCatherLib]
var litNameArray = ['A.V. Sorensen Public Library','Benson Public Library','Bess Johnson Public Library','Charles B. Washington Public Library','Florence Public Library','Millard Public Library','Milton R. Abrahams Public Library','Saddlebrook Public Library','South Omaha Public Library','W.Clarke Swanson Public Library','W. Dale Clark Public Library','Willa Cather Public Library']

var musicArray = [barleyStreet,maha,oleavers,reverb,slowdown,sokol,waitingRoom];
var musicNameArray = ['Barley Street Tavern','Maha Music Festival','OLeavers','Reverb Lounge','Slowdown','Sokol Underground','The Waiting Room'];

var theatreArray = [blueBarn,circle,funnyBone,holland,omahaPlayhouse,orpheum,ralstonTheatre,shelterbelt,backline,theRose];
var theatreNameArray = ['Blue Barn Theatre','Circle Theatre','Funny Bone Comedy Club','Holland Center','Omaha Community Playhouse','Orpheum Theater','Ralston Community Theatre','Shelterbelt Theatre','The Backline Comedy Club','The Rose'];

//Location Markers
    var artPinImage = new google.maps.MarkerImage("images/art.png",
        new google.maps.Size(32, 38),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
				
    var culturePinImage = new google.maps.MarkerImage("images/culture.png",
        new google.maps.Size(32, 38),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
				
    var litPinImage = new google.maps.MarkerImage("images/lit2.png",
        new google.maps.Size(32, 38),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));				

    var musicPinImage = new google.maps.MarkerImage("images/music.png",
        new google.maps.Size(32, 38),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));
				
    var theatrePinImage = new google.maps.MarkerImage("images/theatre.png",
        new google.maps.Size(32, 38),
        new google.maps.Point(0,0),
        new google.maps.Point(10, 34));				

	
	//I WANT IT ALL
	function iWantItAll(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
		zoom: 12,
		center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
		//Show Art AND
		var artCoord;
		for (artCoord in artArray){
			new google.maps.Marker({
				position: artArray[artCoord],
				map: map,
				icon: artPinImage,
				title: artNameArray[artCoord]
		});
		}	
		
		//Show Culture AND
		var cultureCoord;
		for (cultureCoord in cultureArray){
			new google.maps.Marker({
				position: cultureArray[cultureCoord],
				map: map,
				icon: culturePinImage,
				title: cultureNameArray[cultureCoord]
		});
		}
		
		//Show Literature AND
		var litCoord;
		for (litCoord in litArray){
			new google.maps.Marker({
				position: litArray[litCoord],
				map: map,
				icon: litPinImage,
				title: litNameArray[litCoord]
		});
		}	
		
		//Show Music AND
		var musicCoord;
		for (musicCoord in musicArray){
			new google.maps.Marker({
				position: musicArray[musicCoord],
				map: map,
				icon: musicPinImage,
				title: musicNameArray[musicCoord]
		});
		}
		
		//Show Theatre
		var theatreCoord;
		for (theatreCoord in theatreArray){
			new google.maps.Marker({
				position: theatreArray[theatreCoord],
				map: map,
				icon: theatrePinImage,
				title: theatreNameArray[theatreCoord]
		});
		}

	}

	function showMeArt(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
			zoom: 12,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var artCoord;
		for (artCoord in artArray){
			new google.maps.Marker({
				position: artArray[artCoord],
				map: map,
				icon: artPinImage,
				title: artNameArray[artCoord]
		});
		}
	}

	function showMeCulture(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
			zoom: 12,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var cultureCoord;
		for (cultureCoord in cultureArray){
			new google.maps.Marker({
				position: cultureArray[cultureCoord],
				map: map,
				icon: culturePinImage,
				title: cultureNameArray[cultureCoord]
		});
		}
	}

	function showMeLit(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
			zoom: 12,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var litCoord;
		for (litCoord in litArray){
			new google.maps.Marker({
				position: litArray[litCoord],
				map: map,
				icon: litPinImage,
				title: litNameArray[litCoord]
		});
		}
	}

	function showMeMusic(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
			zoom: 12,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var musicCoord;
		for (musicCoord in musicArray){
			new google.maps.Marker({
				position: musicArray[musicCoord],
				map: map,
				icon: musicPinImage,
				title: musicNameArray[musicCoord]
			});
		}
	}

	function showMeTheatre(){
		var myLatlng = new google.maps.LatLng(41.274818, -96.077010);
		var mapOptions = {
			zoom: 12,
			center: myLatlng
		}
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		var theatreCoord;
		for (theatreCoord in theatreArray){
			new google.maps.Marker({
				position: theatreArray[theatreCoord],
				map: map,
				icon: theatrePinImage,
				title: theatreNameArray[theatreCoord]
			});
		}
	}
	
	function toggleMap(id){
		var e = document.getElementById(id);
		  if(e.style.visibility == 'visible')
			e.style.visibility = 'hidden';
		  else
			e.style.visibility = 'visible';
	}
				
  
google.maps.event.addDomListener(window, 'load', iWantItAll);				