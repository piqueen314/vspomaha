﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using VspOmaha.Data;
using views = VspOmaha.ViewModels;
using models = VspOmaha.Models;
using AutoMapper;
using VspOmaha.Utility;
using VspOmaha.ViewModels.Home;

namespace VspOmaha.Controllers
{
  public class HomeController : Controller
  {
    public ActionResult Index()
    {
      //get attractions
      var repo = new AttractionRepository();
      var attractions = repo.GetAttractions();

      //turn attractions into the view with automapper
      var attractionViews = Mapper.Map<List<models.Attraction>, List<views.Home.AttractionView>>(attractions);

      //split them up by type and hook them up to the view
      var homeView = new HomeView
      {
        ArtAttractions = attractionViews.Where(x => x.AttractionTypeId == (int)Lookups.AttractionTypes.Art).ToList(),
        CultureAttractions = attractionViews.Where(x => x.AttractionTypeId == (int)Lookups.AttractionTypes.Culture).ToList(),
        LiteratureAttractions = attractionViews.Where(x => x.AttractionTypeId == (int)Lookups.AttractionTypes.Literature).ToList(),
        MusicAttractions = attractionViews.Where(x => x.AttractionTypeId == (int)Lookups.AttractionTypes.Music).ToList(),
        TheatreAttractions = attractionViews.Where(x => x.AttractionTypeId == (int)Lookups.AttractionTypes.Theatre).ToList()
      };

      return View("Index", homeView);
    }

    [HttpGet]
    public JsonResult GetAttraction(int attractionId)
    {
      var repo = new AttractionRepository();
      var attraction = repo.GetAttraction(attractionId);
      var attractionView = Mapper.Map<models.Attraction, views.Home.AttractionView>(attraction);

      return Json(attractionView, JsonRequestBehavior.AllowGet);
    }

    public ActionResult About()
    {
      ViewBag.Message = "Your application description page.";

      return View();
    }

    public ActionResult Contact()
    {
      ViewBag.Message = "Your contact page.";

      return View();
    }

    public ActionResult map()
    {
      return View();
    }

  }
}
