﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VspOmaha.Data;
using models = VspOmaha.Models;
using views = VspOmaha.ViewModels;

namespace VspOmaha.Controllers
{

  [Authorize]
  public class AttractionController : Controller
  {
    /// <summary>
    /// get attraction index page
    /// </summary>
    /// <returns></returns>
    /// 
    [HttpGet]
    public ActionResult Index()
    {
      var attractionRepo = new AttractionRepository();
      var attractions = attractionRepo.GetAttractions();
      var attractionViews = Mapper.Map<List<models.Attraction>, List<views.Attraction>>(attractions);

      return View(attractionViews);
    }

    /// <summary>
    /// get attraction create page with a new attraction view
    /// </summary>
    /// <returns></returns>
    public ActionResult Create(int? attractionId = null)
    {
      var attractionView = new views.Attraction();
      var attractionRepo = new AttractionRepository();

      if (attractionId.HasValue)
      {
        //edit an attraction
        var attraction = attractionRepo.GetAttraction(attractionId.Value);

        attractionView = Mapper.Map<models.Attraction, views.Attraction>(attraction);
      }
      else
      {
        //new attraction
        var neighborhoods = attractionRepo.GetNeighborhoods();

        var attractionNeighborhoods = new List<views.AttractionNeighborhood>();

        foreach (var hood in neighborhoods)
        {
          var ahood = new views.AttractionNeighborhood
          {
            IsSelected = false,
            Neighborhood = Mapper.Map<models.Neighborhood, views.Neighborhood>(hood),
            NeighborhoodId = hood.NeighborhoodId
          };

          attractionNeighborhoods.Add(ahood);
        }

        attractionView.AttractionNeighborhoods = attractionNeighborhoods;
      }

      var attractionTypes = attractionRepo.GetAttractionTypes();

      var attractionTypesSelectList = attractionTypes
        .OrderBy(x => x.Name)
        .Select(x => new SelectListItem
        {
          Text = x.Name,
          Value = x.AttractionTypeId.ToString()
        })
        .ToList();

      attractionView.AttractionTypes = attractionTypesSelectList;

      return View("Create", attractionView);
    }

    [HttpPost]
    public ActionResult Create(views.Attraction attractionView)
    {
      var attractionData = Mapper.Map<views.Attraction, models.Attraction>(attractionView);
      var attractionRepo = new AttractionRepository();

      try
      {
        attractionRepo.ModifyAttraction(attractionData);
      }
      catch (Exception)
      {
        throw;
      }

      return RedirectToAction("Index", "Attraction");
    }

    [HttpGet]
    public ActionResult Details(int attractionId)
    {
      var attractionView = new views.Attraction();
      var attractionRepo = new AttractionRepository();
      var attraction = attractionRepo.GetAttraction(attractionId);

      attractionView = Mapper.Map<models.Attraction, views.Attraction>(attraction);

      return View("Details", attractionView);
    }

    public ActionResult Delete(int attractionId)
    {
      var attractionRepo = new AttractionRepository();

      attractionRepo.DeleteAttraction(attractionId);

      return RedirectToAction("Index", "Attraction");
    }
  }
}