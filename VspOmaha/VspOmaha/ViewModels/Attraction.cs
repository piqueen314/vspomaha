﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using VspOmaha.Models;
using VspOmaha.Utility;

namespace VspOmaha.ViewModels
{
  public class Attraction
  {
    public int AttractionId { get; set; }

    public int AddressId { get; set; }

    public int AttractionTypeId { get; set; }

    public IEnumerable<SelectListItem> AttractionTypes { get; set; }

    [Required]
    public string Name { get; set; }

    public string Url { get; set; }

    [Required]
    public string Synopsis { get; set; }

    public string ImagePath { get; set; }

    public Address Address { get; set; }

    public List<AttractionNeighborhood> AttractionNeighborhoods { get; set; }

    public List <Neighborhood> Neighborhoods { get; set; }
  }
}
