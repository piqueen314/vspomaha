﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VspOmaha.ViewModels
{
  public class AttractionNeighborhood
  {
    public int AttractionNeighborhoodId { get; set; }

    public int AttractionId { get; set; }

    public int NeighborhoodId { get; set; }

    public bool IsSelected { get; set; }

    public Neighborhood Neighborhood { get; set; }
  }
}