﻿using System.Collections.Generic;

namespace VspOmaha.ViewModels
{
  public class Neighborhood
  {
    public int NeighborhoodId { get; set; }

    public string Name { get; set; }

    public List<Neighborhood> Neighborhoods { get; set; }
  }
}