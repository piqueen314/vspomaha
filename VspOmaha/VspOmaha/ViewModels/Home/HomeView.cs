﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VspOmaha.ViewModels.Home
{
  public class HomeView
  {
    public List<AttractionView> ArtAttractions { get; set; }

    public List<AttractionView> CultureAttractions { get; set; }

    public List<AttractionView> LiteratureAttractions { get; set; }

    public List<AttractionView> MusicAttractions { get; set; }

    public List<AttractionView> TheatreAttractions { get; set; }
  }
}
