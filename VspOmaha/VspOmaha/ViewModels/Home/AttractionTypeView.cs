﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VspOmaha.ViewModels.Home
{
  public class AttractionTypeView
  {
    public int AttractionId { get; set; }

    public int AttractionTypeId { get; set; }

    public string Name { get; set; }
  }
}
