﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web.Mvc;

namespace VspOmaha.Utility
{
  public static class Lookups
  {
    public enum AttractionTypes  
    {
      [Description("Art")]
      Art = 1,

      [Description("Culture")]
      Culture = 2,

      [Description("Literature")]
      Literature = 3,

      [Description("Music")]
      Music = 4,

      [Description("Theatre")]
      Theatre = 5
    }

    public static IEnumerable<SelectListItem> GetAttractionTypeList()
    {
      var attractionTypeList = new List<SelectListItem>();

      foreach (AttractionTypes at in (AttractionTypes[])Enum.GetValues(typeof(AttractionTypes)))
      {
        attractionTypeList.Add(new SelectListItem { Text = at.ToString(), Value = ((int)at).ToString() });
      }

      return attractionTypeList.AsEnumerable();
    }

    public enum Neighborhoods
    {
      [Description("Aksarben")]
      Aksarben = 1,

      [Description("Benson")]
      Benson = 2,

      [Description("Central Omaha")]
      CentralOmaha = 3,

      [Description("Deer Park")]
      DeerPark = 4,

      [Description("Downtown")]
      Downtown = 5,

      [Description("Dundee")]
      Dundee = 6,

      [Description("Hanscom Park")]
      HanscomPark = 7,

      [Description("Midtown")]
      Midtown = 8,

      [Description("North Omaha")]
      NorthOmaha = 9,

      [Description("Old Market")]
      OldMarket = 10,

      [Description("South Omaha")]
      SouthOmaha = 11,

      [Description("West Omaha")]
      WestOmaha = 12
    }

    public static IEnumerable<SelectListItem> GetNeighborhoodList()
    {
      var neighborhoodList = new List<SelectListItem>();

      foreach (Neighborhoods nl in (Neighborhoods[])Enum.GetValues(typeof(Neighborhoods)))
      {
        neighborhoodList.Add(new SelectListItem { Text = nl.ToString(), Value = ((int)nl).ToString() });
      }

      return neighborhoodList.AsEnumerable();
    }

    public static string GetDescription(Enum en)
    {
      var type = en.GetType();
      var memberInfo = type.GetMember(en.ToString());

      if (memberInfo != null && memberInfo.Length > 0)
      {
        var attrs = memberInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

        if (attrs != null && attrs.Length > 0)
        {
          return ((DescriptionAttribute)attrs[0]).Description;
        }
      }
      return en.ToString();
    }
  }
}