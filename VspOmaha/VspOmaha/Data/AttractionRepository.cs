﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using VspOmaha.Models;

namespace VspOmaha.Data
{
  public class AttractionRepository
  {
    public Attraction ModifyAttraction(Attraction attraction)
    {
      using (var db = new OmahaDbContext())
      {
        if(attraction.AttractionId == 0)
        {
          //new
          db.Attractions.Add(attraction);
        }
        else
        {
          //edit
          db.Attractions.Attach(attraction);

          db.Entry(attraction).State = EntityState.Modified;
        }

        db.SaveChanges();
      }

      return attraction;
    }

    public List<Attraction> GetAttractions()
    {
      var attractions = new List<Attraction>();
      
      using (var db = new OmahaDbContext())
      {
        attractions = db.Attractions
          .Include(x => x.Address)
          .Include(x => x.AttractionNeighborhoods)
          .Include(x => x.AttractionNeighborhoods.Select(y => y.Neighborhood))
          .ToList();
      }
      return attractions;
    }

    public Attraction GetAttraction(int attractionId)
    {
      var attraction = new Attraction();

      using (var db = new OmahaDbContext())
      {
        attraction = db.Attractions
          .Include(x => x.Address)
          .Include(x => x.AttractionType)
          .Include(x => x.AttractionNeighborhoods)
          .Include(x => x.AttractionNeighborhoods.Select(y => y.Neighborhood))
          .Single();
      }

      return attraction;
    }

    public List<AttractionType> GetAttractionTypes()
    {
      var attractionTypes = new List<AttractionType>();

      using (var db = new OmahaDbContext())
      {
        attractionTypes = db.AttractionTypes.ToList();
      }

      return attractionTypes;
    }

    public List<Neighborhood> GetNeighborhoods()
    {
      var neighborhoods = new List<Neighborhood>();

      using (var db = new OmahaDbContext())
      {
        neighborhoods = db.Neighborhoods.ToList();
      }

      return neighborhoods;
    }

    public void DeleteAttraction(int attractionId)
    {
      using (var db = new OmahaDbContext())
      {
        var attraction = db.Attractions.Single(x => x.AttractionId == attractionId);
        
        db.Attractions.Remove(attraction);

        db.SaveChanges();
      }
    }
  }
}