﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using VspOmaha.Models;

namespace VspOmaha.Data
{
  class OmahaDbContext : IdentityDbContext<ApplicationUser>
	{
    public OmahaDbContext() : base("name = OmahaContext")
    {
    }

    public static OmahaDbContext Create()
    {
      return new OmahaDbContext();
    }

    public DbSet<Address> Addresses { get; set; }
    public DbSet<Attraction> Attractions { get; set; }
    public DbSet<AttractionNeighborhood>  AttractionNeighborhoods {get; set;}
    public DbSet<AttractionType> AttractionTypes{ get; set; }
    public DbSet<Neighborhood> Neighborhoods { get; set; }
  }
}
