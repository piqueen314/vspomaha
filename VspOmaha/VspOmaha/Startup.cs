﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VspOmaha.Startup))]
namespace VspOmaha
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
