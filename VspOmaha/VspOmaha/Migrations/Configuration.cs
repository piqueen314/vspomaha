namespace VspOmaha.Migrations
{
  using System;
  using System.Data.Entity.Migrations;
  using System.Linq;
  using VspOmaha.Models;
  using VspOmaha.Utility;

  internal sealed class Configuration : DbMigrationsConfiguration<VspOmaha.Data.OmahaDbContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(VspOmaha.Data.OmahaDbContext context)
    {
      SeedAttractionTypes(context);
      SeedNeighborhoods(context);
      //SeedThings
      //SeedThings2

    }

    private void SeedAttractionTypes(VspOmaha.Data.OmahaDbContext db)
    {
      try
      {
        foreach (Lookups.AttractionTypes atType in (Lookups.AttractionTypes[])Enum.GetValues(typeof(Lookups.AttractionTypes)))
        {
          SeedAttractionType(db, (int)atType, Lookups.GetDescription(atType));
        }
      }
      catch (Exception)
      {

        throw;
      }
    }

    private void SeedAttractionType(VspOmaha.Data.OmahaDbContext db, int attractionTypeID, string attractionTypeName)
    {
      /*
       * check to see if there's an attraction type in the db with the given attractionTypeId
       * 
       * if isn't in there add it to the database
       * 
       * if it is in there, re-set the name property (in case it was renamed in the enum)
       */

      ///get the attractiontype by id
      var aType = db.AttractionTypes.SingleOrDefault(x => x.AttractionTypeId == attractionTypeID);

      if (aType == null)
      {
        //we didn't get one, create one
        db.AttractionTypes.Add(new AttractionType
        {
          AttractionTypeId = attractionTypeID,
          Name = attractionTypeName
        });
      }
      else
      {
        //we got one, re-set the name
        aType.Name = attractionTypeName;
      }
    }

    private void SeedNeighborhoods(VspOmaha.Data.OmahaDbContext db)
    {
      try
      {
        foreach (Lookups.Neighborhoods neighbohood in (Lookups.Neighborhoods[])Enum.GetValues(typeof(Lookups.Neighborhoods)))
        {
          SeedNeighborhood(db, (int)neighbohood, Lookups.GetDescription(neighbohood));
        }
      }
      catch (Exception)
      {

        throw;
      }
    }

    private void SeedNeighborhood(VspOmaha.Data.OmahaDbContext db, int neighborhoodId, string neighborhoodName)
    {
      /*
       * check to see if there's an attraction type in the db with the given attractionTypeId
       * 
       * if isn't in there add it to the database
       * 
       * if it is in there, re-set the name property (in case it was renamed in the enum)
       */

      ///get the neighborhood by id
      var neighbohood = db.Neighborhoods.SingleOrDefault(x => x.NeighborhoodId == neighborhoodId);

      if (neighbohood == null)
      {
        //we didn't get one, create one
        db.Neighborhoods.Add(new Neighborhood
        {
          NeighborhoodId = neighborhoodId,
          Name = neighborhoodName
        });
      }
      else
      {
        //we got one, re-set hte name
        neighbohood.Name = neighborhoodName;
      }
    }
  }
}
