namespace VspOmaha.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class HelloWorld : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Attractions", "Neighborhood_NeighborhoodId", "dbo.Neighborhoods");
            DropIndex("dbo.Attractions", new[] { "Neighborhood_NeighborhoodId" });
            AddColumn("dbo.AttractionNeighborhoods", "IsSelected", c => c.Boolean(nullable: false));
            DropColumn("dbo.Attractions", "Neighborhood_NeighborhoodId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Attractions", "Neighborhood_NeighborhoodId", c => c.Int());
            DropColumn("dbo.AttractionNeighborhoods", "IsSelected");
            CreateIndex("dbo.Attractions", "Neighborhood_NeighborhoodId");
            AddForeignKey("dbo.Attractions", "Neighborhood_NeighborhoodId", "dbo.Neighborhoods", "NeighborhoodId");
        }
    }
}
