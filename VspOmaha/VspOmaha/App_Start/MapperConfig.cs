﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using views = VspOmaha.ViewModels;
using models = VspOmaha.Models;

namespace VspOmaha.App_Start
{
  public static class MapperConfig
  {
    public static void RegisterMaps()
    {
      Mapper.CreateMap<models.Attraction, views.Attraction>();
      Mapper.CreateMap<views.Attraction, models.Attraction>();

      Mapper.CreateMap<models.Address, views.Address>();
      Mapper.CreateMap<views.Address, models.Address>();

      Mapper.CreateMap<models.AttractionNeighborhood, views.AttractionNeighborhood>();
      Mapper.CreateMap<views.AttractionNeighborhood, models.AttractionNeighborhood>();

      Mapper.CreateMap<models.Neighborhood, views.Neighborhood>();
      Mapper.CreateMap<views.Neighborhood, models.Neighborhood>();

      Mapper.CreateMap<models.AttractionType, views.Home.AttractionTypeView>();
      Mapper.CreateMap<views.Home.AttractionTypeView, models.AttractionType>();

      Mapper.CreateMap<models.Attraction, views.Home.AttractionView>();
      Mapper.CreateMap<views.Home.AttractionView, models.Attraction>();
    }
  }
}