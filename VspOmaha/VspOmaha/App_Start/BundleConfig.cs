﻿using System.Web;
using System.Web.Optimization;

namespace VspOmaha
{
  public class BundleConfig
  {
    private static readonly string[] Bootstrap = 
    {
      "~/Scripts/bootstrap*",
      "~/Scripts/respond*"
    };

    private static readonly string[] Jquery = 
    {
      "~/Scripts/jquery-{version}.js"
    };

    private static readonly string[] JqueryVal = 
    {
      "~/Scripts/jquery.validate*"
    };

    private static readonly string[] Modernizr = 
    {
      "~/Scripts/modernizr-*"
    };

    private static readonly string[] Css = 
    {
      "~/Content/bootstrap.css",
      "~/Content/site.css"
    };

    private static readonly string[] CssEx = 
    {
      "~/Content/admin.css",
      "~/Content/cati.css",
      "~/Content/catiModal.css",
      "~/Content/example.css",
      "~/Content/map.css",
      "~/Content/slick.css",
      "~/Content/jqueryMegaMenu.css",
      "~/Content/Style2.css"
    };

    private static readonly string[] ScriptsEx = 
    {
      "~/Scripts/admin.js",
      "~/Scripts/map.js",
      "~/Scripts/modal.js",
      "~/Scripts/scripts.js",
      "~/Scripts/slick.js",
      "~/Scripts/jqueryMegaMenu.js"
    };

    private static readonly string[] Utility =
    {
      "~/Scripts/Utility/DropDownList*"
    };
   
    private static readonly string[] AttractionCreate=
    {
      "~/Scripts/Views/Attraction/Create*"
    };

    private static readonly string[] AttractionEdit =
    {
      "~/Scripts/Views/Attraction/Edit*"
    };

    private static readonly string[] HomeIndex =
    {
      "~/Scripts/Views/Home/Index*"
    };

    // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      // Set EnableOptimizations to false for debugging. For more information,
      // visit http://go.microsoft.com/fwlink/?LinkId=301862
#if DEBUG
      BundleTable.EnableOptimizations = false;
#else
      BundleTable.EnableOptimizations = true;
#endif

      bundles.Add(new StyleBundle("~/Content/Layout").Include(Css));
      //bundles.Add(new StyleBundle("~/Content/Layout").Include(Css).Include(CssEx));

      bundles.Add(new ScriptBundle("~/Bundles/Layout").Include(Modernizr).Include(Jquery).Include(JqueryVal)
            .Include(Bootstrap).Include(ScriptsEx).Include(Utility));

      bundles.Add(new ScriptBundle("~/Bundles/Attraction/Create").Include(Utility).Include(AttractionCreate));

      bundles.Add(new ScriptBundle("~/Bundles/Attraction/Edit").Include(Utility).Include(AttractionEdit));

      bundles.Add(new ScriptBundle("~/Bundles/Home/Index").Include(HomeIndex));
    }
  }
}
