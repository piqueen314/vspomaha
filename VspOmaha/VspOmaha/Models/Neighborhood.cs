﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VspOmaha.Models
{
  public class Neighborhood
  {
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int NeighborhoodId { get; set; }

    [Required]
    public string Name { get; set; }
  }
}