﻿
namespace VspOmaha.Models
{
  public class AttractionNeighborhood
  {
    public int AttractionNeighborhoodId { get; set; }

    public int AttractionId { get; set; }

    public int NeighborhoodId { get; set; }

    public bool IsSelected { get; set; }

    public virtual Attraction Attraction { get; set; }

    public virtual Neighborhood Neighborhood { get; set; }
  }
}