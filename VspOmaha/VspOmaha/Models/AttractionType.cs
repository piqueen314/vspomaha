﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VspOmaha.Models
{
  public class AttractionType
  {
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public int AttractionTypeId { get; set; }

    [Required]
    public string Name { get; set; }

  }
}