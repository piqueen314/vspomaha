﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace VspOmaha.Models
{
  public class Attraction
  {
    public int AttractionId { get; set; }

    public int AddressId { get; set; }

    public int AttractionTypeId { get; set; }

    [Required]
    public string Name { get; set; }

    public string Url { get; set; }

    [Required]
    public string Synopsis { get; set; }

    public string ImagePath { get; set; }

    public virtual Address Address { get; set; }

    public virtual AttractionType AttractionType { get; set; }

    public virtual ICollection<AttractionNeighborhood> AttractionNeighborhoods { get; set; }

  }
}