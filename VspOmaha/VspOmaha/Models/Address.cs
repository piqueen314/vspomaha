﻿using System.ComponentModel.DataAnnotations;

namespace VspOmaha.Models
{
  public class Address
  {
    public int AddressId { get; set; }

    [Required]
    public string Address1 { get; set; }

    public string Address2 { get; set; }

    [Required]
    public string City { get; set; }
    
    [Required]
    public string State { get; set; }

    [Required]
    public string Zip { get; set; }
  }
}